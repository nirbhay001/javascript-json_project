# Word Master Game

## In this project, I have made a word game project by using:
* HTML
* CSS
* JavaScript
* API
## Project Description
* In this project, I have made a word game project using HTML, CSS, JavaScript and API with a beautiful layout and user interface.
* In this game, you have to choose the right word by correcting yourself at every stage of the game.
* There are Six attempts to choose the correct word, and the word would be five characters long.
* If you choose the right character at the right index, then the box color will be changed to green.
* If the box color is showing yellow, means that the character will be inside the game, otherwise that character does not exist in the word.
## Visit this site to play the game.
* https://jolly-fairy-b995fe.netlify.app/
